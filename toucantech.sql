/**
 * Author:  myriam
 * Created: 06-Feb-2017
 */

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `toucantech`

-- --------------------------------------------------------

-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `schools` (
    `id` int(5) NOT NULL AUTO_INCREMENT,
    `name` varchar(30) COLLATE UTF8_GENERAL_CI NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=UTF8_GENERAL_CI AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE UTF8_GENERAL_CI NOT NULL,
  `email` varchar(30) COLLATE UTF8_GENERAL_CI NOT NULL UNIQUE,
  `school_id` int(5) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`school_id`)
        REFERENCES schools(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=UTF8_GENERAL_CI AUTO_INCREMENT=1 ;

--
-- Dumping data for table `members`
--
INSERT INTO `schools` (`name`) VALUES
('Abberley Hall'),
('Watford GS for Girls'),
('Concord College'),
('Roedean School'),
('University of Sussex'),
('Alpha Plus');

INSERT INTO `members` (`name`,`email`,`school_id`) VALUES
('Myriam', 'myriam.puchalt@icloud.com', 1),
('Sabrina', 'myriam.sabrina@icloud.com', 2),
('Carlos', 'myriam.carlos@icloud.com', 3),
('Marco', 'myriam.marco@icloud.com', 4),
('Anna', 'myriam.anna@icloud.com', 5),
('Nik', 'myriam.nik@icloud.com', 6),
('Richard', 'richard@richard.com', 1),
('Joe', 'joe@richard.com', 2),
('Alberto', 'alberto@richard.com', 4),
('Simone', 'sim@gmail.com', 3),
('Ana', 'ana@gmail.com', 5),
('Miki', 'miki@gmail.com', 6);
