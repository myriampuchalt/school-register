   
        function showForm($type){
                $("#result").html('');
                $('#addMember, #listMembers').hide(); 
                $('#' + $type).show();
                listSchools();
        }
        
        
        function listSchools(){
            
            var ajaxRequest;
            ajaxRequest = $.ajax({
                url: "index.php?ctl=listSchools",
                type: "GET"
            });
            
            ajaxRequest.done(function (response){     
                 $(".select").html(response); 
            });
        }
            
        $(document).ready(function() {

            $(".button2").click(function() { 
           
                var ajaxRequest;
                var values = $('#addMember').serialize();
              
                ajaxRequest= $.ajax({
                       url: "index.php?ctl=addMember",
                       type: "POST",
                       data: values
                       });

                ajaxRequest.done(function (){
                     $("#result").html('Your details have been successfully submited ');
                });
                ajaxRequest.fail(function (){      
                     $("#result").html('There was an error submiting your details. Please check them and try again.');
                });          
        });
        
        $(".button3").click(function() { 
           
            var ajaxRequest;
            var values = $('#listMembers').serialize();
            
            ajaxRequest= $.ajax({
                   url: "index.php?ctl=listMembers",
                   type: "POST",
                   data: values,
                   });

            ajaxRequest.done(function (response){     
                 $("#result").html(response); 
            });
            ajaxRequest.fail(function (){
                 $("#result").html('There was an error with your request. Please check your details and try again.');
            });
        });
    });
       