<?php

class viewMembers{
    
    private $members;
    
    public function __construct($members) {
        $this->members = $members;
    }
    
    public function output(){ ?>
        <p>
        <table class="members">
        <tr class="members">
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
        </tr>
<?php foreach($this->members as $member):?>
    <tr class="members">
        <td><?php echo $member['id'];?></td>
        <td><?php echo $member['name'];?></td>
        <td><?php echo $member['email'];?></td>
    </tr>
<?php endforeach;?>
</table>
    <?php }
    
}

