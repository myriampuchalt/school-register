<?php

/**
 * Description of viewSchools
 *
 * @author myriam
 */
class viewSchools {
    
      private $schools;
    
    public function __construct($schools) {
        $this->schools = $schools;
    }
    
    public function output(){ ?>

        <select name="school">
            <?php foreach($this->schools as $school):?>
                <option value="<?php echo $school['name']; ?>"><?php echo $school['name']; ?> </option>
            <?php endforeach;?>
        </select>
    <?php }   
}