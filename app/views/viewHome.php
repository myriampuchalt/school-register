<?php 

class viewHome{

    public function output(){ ?>
        
        <html>
    <head>
        <meta charset="UTF-8">
        <title>School Register</title>
        <meta name="description" content="Application that adds and lists members from Toucan Tech DB">
        <meta name="author" content="Myriam Puchalt Gisep">
        <link type="text/css" rel="stylesheet" href="<?php echo Config::$css ?>" />
        <script type="text/javascript" src="<?php echo Config::$jquery ?>"></script>
        <script type="text/javascript" src="<?php echo Config::$js ?>"></script>     
    </head>
   
    <body>
        
        <div id="homepage">
            
            <h1><img src="toucan.png" /><a class="header" href="index.php?ctl=home">School Register</a></h1>
        
            <div id="content">
                <button class="button1" type="button" onclick="showForm('addMember');">Add Member</button>
                <button class="button1" type="button" onclick="showForm('listMembers');">List all Members</button>
           
                <form id="addMember" method="POST">
                    <h3>Member details:</h3>
                    <div>               
                        <span class="span1">Name: <input name="name" class="input1" type="text" placeholder="Full Name" required /></span><p>
                        <span class="span1">Email: <input name="email" class="input1" type="text" placeholder="Email" required /></span><p>
                        <span class="span1">School:
                        <span class="select"></span><p>
                        <button class="button2" type="button" form="addMember">Add Member</button>
                    </div>
                </form>
                                        
                <form id="listMembers" method="POST">
                    <h3>List all members for a selected school:</h3>
                    <div>  
                        <span class="select"></span><p>
                        <button class="button3" type="button" form="listMembers">Show all</button>
                    </div>
                </form>
                    
                <div id="result">
                    
                        <!-- The result of the request rendered inside this div -->
                </div>
            
            </div> 
            
        </div>
            
    </body>
</html>

<?php
    
    }
}


