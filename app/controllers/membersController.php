<?php

/**
 * Description of Controller
 * @author myriam
 */

require_once 'app/views/viewMembers.php';

class membersController {
    
    public $model;
    public $view;
        
    function __construct() {
        $this->model = new Model ();
    }

    public function addMember() {
                    
        $name = filter_input(INPUT_POST, 'name');
        $email = filter_input(INPUT_POST, 'email');
        $school = filter_input(INPUT_POST, 'school');  
                    
        if ((!($name || $email)) || (!filter_var($email, FILTER_VALIDATE_EMAIL))) {
            http_response_code(400);
        } else {
            $this->model->addMember ( $name, $email, $school );
        }		
    }
        
    public function listMembers() {
        $school = filter_input(INPUT_POST, 'school');                
	$result = $this->model->listMembers ($school);                                
        $this->view = new viewMembers($result);
        $this->view->output();                               
    }
        
}
