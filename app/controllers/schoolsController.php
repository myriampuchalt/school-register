<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of schoolsController
 *
 * @author myriam
 */

require_once 'app/views/viewSchools.php';

class schoolsController {
    
    public $model;
    public $view;
    
    // Constructor
    
    function __construct() {
        $this->model = new Model ();
    }
   
    public function listSchools() {
        $result = $this->model->listSchools();
        $this->view = new viewSchools($result);
        $this->view->output();
    }
    
    public function insertSchool() {
        
        $school = filter_input(INPUT_POST, 'school');  
        $this->model->addSchool ( $school );
        
    }
}
