<?php

/**
 * Description of homeController
 *
 * @author myriam
 */

require_once 'app/views/viewHome.php';

class homeController {
   
    public $view;
    
    function showHome(){
        
        $this->view = new viewHome();
        $this->view->output();
    }
    
}
