<?php

/**
 * Description of Model
 * @author myriam
 */

class Model {
    
    private $connection;
    
    function connect(){
         
        $dsn = 'mysql:host=' . Config::$host . ';dbname='.Config::$db;
        
        try {
            $this->connection = new PDO($dsn, Config::$db_user, Config::$db_password);
        } catch (PDOException $err){
            echo 'Connection failed: ' . $err->getMessage();
            file_put_contents('PDOerrors.txt', $err, FILE_APPEND); //error_log
            die(); 
        }
    }
    
    function getSchoolId($school){
        
        $this->connect();
        $sql = 'SELECT id FROM schools WHERE name LIKE :school LIMIT 1';
        $stmt = $this->connection->prepare($sql);
        $school = '%'.$school.'%';
        $stmt->bindParam(':school', $school, PDO::PARAM_STR);
        $stmt->execute();
        while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
            $school_id [] = $result;
        }
        $this->connection = NULL;
        
        return (int)implode($school_id[0]);   
    }
    
    public function addSchool($school) {
        
        $this->connect();
        $sql = "INSERT INTO schools (name) VALUES (:name)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':school', $school, PDO::PARAM_STR);
        $stmt->execute();
        $this->connection = NULL;
        
    }
    
    function addMember($name, $email, $school){
        
        $school_id = $this->getSchoolId($school);        
        $this->connect();

        $sql = "INSERT INTO members (name, email, school_id) VALUES (:name, :email, :school_id)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':school_id', $school_id, PDO::PARAM_INT);
        $stmt->execute();       
        $this->connection = NULL;
    }
    
    function listMembers($school){ 

        $school_id = $this->getSchoolId($school); 
        $this->connect();
        $sql = "SELECT * FROM members WHERE school_id= :school_id ORDER BY id ASC";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':school_id', $school_id, PDO::PARAM_INT);
        $stmt->execute(); 
        while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
            $members [] = $result;
        }
        $this->connection = NULL;
        
        return $members;
            
    }
    
    function listSchools(){
        
        $this->connect();
        $sql = "SELECT name FROM schools";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
            $schools [] = $result;
        }
        $this->connection = NULL;
        return $schools;
        
    }
}
