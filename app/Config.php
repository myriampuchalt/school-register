<?php

// Configuration Variables

class Config {
    
    public static $host = "localhost";
    public static $db = "toucantech";
    public static $db_user = "root";
    public static $db_password = "root";
    public static $css = "style.css";
    public static $jquery = "https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js";
    public static $js = "script.js";
    
}

