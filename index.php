<?php

// This is the Front Controller (Entry point)
// It handles every request for all the other parts of the application

// Templating & Routing

require_once 'app/Model.php';
require_once 'app/controllers/homeController.php';
require_once 'app/controllers/membersController.php';
require_once 'app/controllers/schoolsController.php';
require_once 'app/Config.php';

// we create the routes

$map = array (
		
		'home' => array (
				'controller' => 'homeController',
				'action' => 'showHome' 
		),
		'addMember' => array (
				'controller' => 'membersController',
				'action' => 'addMember' 
		),
		'listMembers' => array (
				'controller' => 'membersController',
				'action' => 'listMembers' 
		),
                'listSchools' => array (
                                'controller' => 'schoolsController',
                                'action' =>  'listSchools'
                )
);

// We parse the routes

if (isset ( $_GET ['ctl'] )) {
	if (isset ( $map [$_GET ['ctl']] )) {
		$route = $_GET ['ctl'];
	} else {
		header ( 'Status: 404 Not Found' );
		echo '<html><body><h1>Error 404: Page Not Found <i> </body></html>';
		exit ();
	}
} else {
	$route = 'home';
}

$controller = $map [$route];

// We run the specific controller asociated with the selected route

if (method_exists ( $controller  ['controller'], $controller  ['action'] )) {
	call_user_func ( array (
			new $controller  ['controller'] (),
			$controller  ['action'] 
	) );
} else {
	header ( 'Status: 404 Not Found' );
	echo '<html><body><h1>Error 404: Page Not found <i> </body></html>';
        exit();
}
